<?php get_header(); ?>

<div id="content" class="container-fluid site-content">
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 col-lg-4 offset-lg-1">
						<h1><?php the_field('homepagetitle'); ?></h1>
						<div class="homepagetext"><?php the_field('homepagetext'); ?></div>
					</div>
					<div class="col-md-12 col-lg-5 offset-lg-1">
						<?php the_field('homepagevideo'); ?>
					</div>
				</div><!-- row -->
				<?php if(wp_is_mobile()) : ?>
					<?php club_loginregister_nav(); ?>
				<?php endif; ?>
			</div>
		</main><!-- #main -->
	</section><!-- #primary -->
</div><!-- content -->
 
<div class="mainbody" style="background-color:#F4E2DB;">
	
	<div class="container-fluid">        

		<?php $partners = new WP_Query(array('post_type' => 'partner', 'posts_per_page' => -1)); ?>
		<?php if($partners->have_posts()): ?>
			<div class="row logo-showcase">
				<div class="col">
					<div class="logo-showcase-carousel marquee">
						<?php while($partners->have_posts()) : $partners->the_post();?>
							<div class="logo-showcase-slide-item">
								<a style="width:170px" href="<?php echo get_the_permalink(); ?>">
									<?php $partner_logo = wp_get_attachment_image_src( get_post_meta( get_the_ID(), 'partner_bio_logo', true ), 'large' ); ?>
									<img src="<?php echo  $partner_logo[0]; ?>" alt="<?php the_title();?>" srcset="" />
								</a>
							</div>
						<?php endwhile; wp_reset_postdata();?>
					</div>
				</div>
			</div><!-- logo-showcase -->
		<?php endif; ?>

		<div class="row">
			<button type="button" id="viewpartners" class="btn btn-primary btn-lg">
				<a href="/partner/">View all partners</a>
			</button>
		</div><!-- row -->

	</div> 

	<div class="container">  
		<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/content-homefeed', 'notitle' );
		endwhile; // End of the loop.
		?>
	</div>

	<div class="container-fluid happening-now">  
		<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/content-offer', 'notitle' );
		endwhile; // End of the loop.
		?>
	</div>

</div><!-- mainbody -->

<?php get_footer(); ?>