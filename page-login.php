<?php
/**
 * Template Name: Login
 */

get_header();
?>
<div class="container">
	<div class="row">
  	<article class="col-sm-10 col-lg-6" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	  
		<?php $enable_vc = get_post_meta(get_the_ID(), '_wpb_vc_js_status', true); ?>
		<?php if(!$enable_vc ): ?>
			<div class="entry-header pt-5">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</div><!-- .entry-header -->
		<?php endif; ?>
		<div class="entry-content">
			<?php
			while ( have_posts() ) : the_post();
			the_content();
			endwhile; // End of the loop.

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-bootstrap-starter' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
	</article><!-- #post-## -->
	</div><!-- row -->
</div>

<?php
get_footer();
