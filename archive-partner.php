<?php
/**
 * The template for displaying archive partners
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
	

<div id="content" class="container-fluid site-content">
	<div class="container">
		<div id="filter">
			<div class="row">
				<div class="col-md-8">
					<h1>Our partners</h1>
				</div>
			</div><!-- row -->
			<div class="row mb-8 pb-5">
				<div class="col-md-8">
					<p>The City A.M Club is your passport to the City, offering you exclusive access to London’s most enticing and exciting experiences.</p>
				</div>
			</div><!-- row -->
			<div class="row">
				<div class="col-md-8">
					<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?><!-- row -->
				</div>
				<div class="col-md-3">
					<span class="postnumber">
						<?php
							$count_posts = wp_count_posts('partner');
							$total_posts = $count_posts->publish;
							echo $total_posts . ' Partners ';
						?>
					</span>
				</div>
			</div><!-- row -->
		</div><!-- filter -->
	</div><!-- container -->
</div><!-- content -->

<div style="width:100%;background-color:#F4E2DB;padding-top:8em;padding-bottom:6em;">
	<div id="partnerpage" class="container alm-listing">
		<?php 
		$args = array( 'post_type' => array('partner'),
			'orderby'        => 'name', 
			'order'          => 'ASC',
			'posts_per_page' => -1 
		);
		$query = new WP_Query( $args );
		?>

			<div class="row">

					<?php $the_query = new WP_Query( $args ); ?>

					<?php if ( $the_query->have_posts() ) : ?>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

							<div class="card col-sm-12 col-md-6 col-lg-6 col-xl-4">
								<a href="<?php the_permalink(); ?>">

									<h2><?php the_title(); ?></h2>

									<div class="partner-thumbnail">
										<img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/partner-thumb.png" alt="Club CityAM Partner" class="img-fluid img-placeholder">
										<?php if ( has_post_thumbnail() ) : ?>
											<?php the_post_thumbnail( "large", array('class'=>'event-img') ); ?>
										<?php endif; ?>										
										<?php $partner_logo = wp_get_attachment_image_src( get_post_meta( get_the_ID(), 'partner_bio_logo', true ), 'large' ); ?>
										<img class="attachment-post-thumbnail" src="<?php echo  $partner_logo[0]; ?>"/>
									</div>

									<!-- start date/time -->
									<?php if (get_field('start_date')): ?>
										<p class="date"><?php the_field('start_date'); ?></p>
									<?php endif; ?>

								</a>
								<!-- tube -->
								<?php if (get_field('nearest_tube')): ?>
									<div style="padding-top:15px;">
										<p class="nearest-tube"><?php the_field('nearest_tube'); ?></p>
									</div>
								<?php endif; ?>
							
								<?php $partner_bio = get_post_meta(get_the_ID(), 'partner_bio', true); ?>
								<p><?php echo $partner_bio; ?></p>
								<span style="margin-top:10px;" class="tellmore"><a class="membership-email" href="<?php the_permalink(); ?>">Tell me more</a></span>
							</div><!-- card -->

						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>

					<?php else : ?>
						<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>

			</div><!-- row -->

	</div><!-- partnerpage -->
</div>

<?php
get_footer();


