<?php
/**
 * WP Bootstrap Starter functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WP_Bootstrap_Starter
 */

add_action('pmxi_update_post_meta', 'my_update_post_meta', 99, 3);
function my_update_post_meta($pid, $m_key, $m_value) {

    if ( $m_key == 'user_tags_tag_id') {

        $interests = array();
        $terms = get_terms( array(
            'taxonomy' => array('tribe_events_cat', 'category'),
            'hide_empty' => false,
        ) );
        $tags = array();
        
        $user_interests = get_user_meta($pid, 'interest',true);
        $interest = '';
        foreach ($terms as $key => $term) {
            $tag_id = get_term_meta($term->term_id,'tag_id',true);

            if($tag_id == $m_value){
                $interest = $term->term_id;
            }
            
        }

        if(is_array($user_interests) && isset($user_interests[0])){
            if(!in_array($interest,$user_interests)){
                $interests[] = $interest;
            }
            $interests = array_merge($user_interests,$interests);
            update_user_meta($pid, 'interest', $interests );
        }else{
            update_user_meta($pid, 'interest', array($m_value) );
        }

    }
}

add_action( 'um_registration_complete', 'my_registration_complete', 10, 2 );
function my_registration_complete( $user_id, $args ) {
    // your code here
    // error_log(print_r($user_id,true));
    // error_log(print_r($args,true));

    $post_tags = get_terms( array(
        'taxonomy' => array('tribe_events_cat', 'category'),
        'hide_empty' => false,
    ) );
    
    $interests = array();
    $event_interests = array();

    foreach ($post_tags as $key => $tag) {
        //error_log(print_r($tag,true));
        if(in_array($tag->name,$args['submitted']['check_boxes_20'])){
            // error_log(print_r($tag->name,true));
            //Add tag to array of interest.
            $interests[] = $tag->term_id;
        }
        if(in_array($tag->name,$args['submitted']['check_boxes_20_22'])){
            // error_log(print_r($tag->name,true));
            //Add tag to array of event interest.
            $event_interests[] = $tag->term_id;

        }
    }
    // Update Interests
	update_user_meta( $user_id, 'interest', $interests );
	// Update Event Interests
	update_user_meta( $user_id, 'event_interest', $event_interests );

	update_user_meta( $user_id, 'job_title', $args['submitted']['your_job_title'] );
	update_user_meta( $user_id, 'gender', $args['submitted']['Gender'] );
	update_user_meta( $user_id, 'billing_company', $args['submitted']['name_of_company'] );
	update_user_meta( $user_id, 'billing_address_1', $args['submitted']['first_line_of_address'] );
	update_user_meta( $user_id, 'billing_city', $args['submitted']['town_city'] );
	update_user_meta( $user_id, 'billing_postcode', $args['submitted']['postcode'] );


    
}

if ( ! function_exists( 'wp_bootstrap_starter_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wp_bootstrap_starter_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on WP Bootstrap Starter, use a find and replace
	 * to change 'wp-bootstrap-starter' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'wp-bootstrap-starter', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
        'primary' => esc_html__( 'Primary', 'wp-bootstrap-starter' ),
        'register' => esc_html__( 'Register', 'wp-bootstrap-starter' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'caption',
	) );

	add_theme_support('custom-logo',array(
        'height' => 50,
        'width' => 200,
        'flex-height' => 50,
        'flex-width' => 200,
	));
	
	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'wp_bootstrap_starter_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

    function wp_boostrap_starter_add_editor_styles() {
        add_editor_style( 'custom-editor-style.css' );
    }
    add_action( 'admin_init', 'wp_boostrap_starter_add_editor_styles' );

}
endif;
add_action( 'after_setup_theme', 'wp_bootstrap_starter_setup' );


/**
 * Add Welcome message to dashboard
 */
function wp_bootstrap_starter_reminder(){
        $theme_page_url = 'https://afterimagedesigns.com/wp-bootstrap-starter/?dashboard=1';

            if(!get_option( 'triggered_welcomet')){
                $message = sprintf(__( 'Welcome to WP Bootstrap Starter Theme! Before diving in to your new theme, please visit the <a style="color: #fff; font-weight: bold;" href="%1$s" target="_blank">theme\'s</a> page for access to dozens of tips and in-depth tutorials.', 'wp-bootstrap-starter' ),
                    esc_url( $theme_page_url )
                );

                printf(
                    '<div class="notice is-dismissible" style="background-color: #6C2EB9; color: #fff; border-left: none;">
                        <p>%1$s</p>
                    </div>',
                    $message
                );
                add_option( 'triggered_welcomet', '1', '', 'yes' );
            }

}
add_action( 'admin_notices', 'wp_bootstrap_starter_reminder' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wp_bootstrap_starter_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wp_bootstrap_starter_content_width', 1170 );
}
add_action( 'after_setup_theme', 'wp_bootstrap_starter_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wp_bootstrap_starter_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'wp-bootstrap-starter' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-starter' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 1', 'wp-bootstrap-starter' ),
        'id'            => 'footer-1',
        'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-starter' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 2', 'wp-bootstrap-starter' ),
        'id'            => 'footer-2',
        'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-starter' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 3', 'wp-bootstrap-starter' ),
        'id'            => 'footer-3',
        'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-starter' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', 'wp_bootstrap_starter_widgets_init' );



/**
 * Enqueue scripts and styles.
 */
function wp_bootstrap_starter_scripts() {


	// load bootstrap css
	wp_enqueue_style( 'wp-bootstrap-starter-bootstrap-css', get_template_directory_uri() . '/inc/assets/css/bootstrap.min.css' );
    // fontawesome cdn
    wp_enqueue_style( 'wp-bootstrap-pro-fontawesome-cdn', 'https://use.fontawesome.com/releases/v5.1.0/css/all.css' );
	// load lightbox styles
    wp_enqueue_style( 'wp-lightbox', get_template_directory_uri() . '/inc/assets/css/jquery.fancybox.min.css' );
	// load bootstrap css

	// load WP Bootstrap Starter styles
	// wp_enqueue_style( 'wp-bootstrap-starter-style', get_stylesheet_uri() );
    if(get_theme_mod( 'theme_option_setting' ) && get_theme_mod( 'theme_option_setting' ) !== 'default') {
        wp_enqueue_style( 'wp-bootstrap-starter-'.get_theme_mod( 'theme_option_setting' ), get_template_directory_uri() . '/inc/assets/css/presets/theme-option/'.get_theme_mod( 'theme_option_setting' ).'.css', false, '' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'poppins-lora') {
        wp_enqueue_style( 'wp-bootstrap-starter-poppins-lora-font', 'https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i|Poppins:300,400,500,600,700' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'montserrat-merriweather') {
        wp_enqueue_style( 'wp-bootstrap-starter-montserrat-merriweather-font', 'https://fonts.googleapis.com/css?family=Merriweather:300,400,400i,700,900|Montserrat:300,400,400i,500,700,800' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'poppins-poppins') {
        wp_enqueue_style( 'wp-bootstrap-starter-poppins-font', 'https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'roboto-roboto') {
        wp_enqueue_style( 'wp-bootstrap-starter-roboto-font', 'https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'arbutusslab-opensans') {
        wp_enqueue_style( 'wp-bootstrap-starter-arbutusslab-opensans-font', 'https://fonts.googleapis.com/css?family=Arbutus+Slab|Open+Sans:300,300i,400,400i,600,600i,700,800' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'oswald-muli') {
        wp_enqueue_style( 'wp-bootstrap-starter-oswald-muli-font', 'https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800|Oswald:300,400,500,600,700' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'montserrat-opensans') {
        wp_enqueue_style( 'wp-bootstrap-starter-montserrat-opensans-font', 'https://fonts.googleapis.com/css?family=Montserrat|Open+Sans:300,300i,400,400i,600,600i,700,800' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'robotoslab-roboto') {
        wp_enqueue_style( 'wp-bootstrap-starter-robotoslab-roboto', 'https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700|Roboto:300,300i,400,400i,500,700,700i' );
    }
    if(get_theme_mod( 'preset_style_setting' ) && get_theme_mod( 'preset_style_setting' ) !== 'default') {
        wp_enqueue_style( 'wp-bootstrap-starter-'.get_theme_mod( 'preset_style_setting' ), get_template_directory_uri() . '/inc/assets/css/presets/typography/'.get_theme_mod( 'preset_style_setting' ).'.css', false, '' );
    }
    //Color Scheme
    /*if(get_theme_mod( 'preset_color_scheme_setting' ) && get_theme_mod( 'preset_color_scheme_setting' ) !== 'default') {
        wp_enqueue_style( 'wp-bootstrap-starter-'.get_theme_mod( 'preset_color_scheme_setting' ), get_template_directory_uri() . '/inc/assets/css/presets/color-scheme/'.get_theme_mod( 'preset_color_scheme_setting' ).'.css', false, '' );
    }else {
        wp_enqueue_style( 'wp-bootstrap-starter-default', get_template_directory_uri() . '/inc/assets/css/presets/color-scheme/blue.css', false, '' );
    }*/

	wp_enqueue_script('jquery');

    // Internet Explorer HTML5 support
    wp_enqueue_script( 'html5hiv',get_template_directory_uri().'/inc/assets/js/html5.js', array(), '3.7.0', false );
    wp_script_add_data( 'html5hiv', 'conditional', 'lt IE 9' );

    wp_enqueue_style( 'wp-bootstrap-lighbox', 'https://code.jquery.com/jquery-3.2.1.min.js' );
 
    if ( is_singular( 'partner' ) ) {
        wp_enqueue_script( 'maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDbUb6X3OuDeKLvzgqhyekKbCAs2k1Ivgk' );
    }

    // wp_enqueue_script('maps', get_template_directory_uri() . '/inc/assets/js/myjs.js', array(), '', true );

    // lighbox js
    wp_enqueue_script('wp-lightbox-js', get_template_directory_uri() . '/inc/assets/js/jquery.fancybox.min.js', array(), '', true );

    wp_enqueue_script('slick-js', get_template_directory_uri() . '/inc/assets/js/slick.min.js', array('jquery'), '', true );
    
    wp_enqueue_script('wp-myjs-js', get_template_directory_uri() . '/inc/assets/js/myjs.js', array('wp-lightbox-js'), '', true );



	// load bootstrap js
    wp_enqueue_script('wp-bootstrap-starter-popper', get_template_directory_uri() . '/inc/assets/js/popper.min.js', array(), '', true );
	wp_enqueue_script('wp-bootstrap-starter-bootstrapjs', get_template_directory_uri() . '/inc/assets/js/bootstrap.min.js', array(), '', true );
    wp_enqueue_script('wp-bootstrap-starter-themejs', get_template_directory_uri() . '/inc/assets/js/theme-script.min.js', array(), '', true );
	wp_enqueue_script( 'wp-bootstrap-starter-skip-link-focus-fix', get_template_directory_uri() . '/inc/assets/js/skip-link-focus-fix.min.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
    }
    
    wp_register_style('main-style', get_template_directory_uri() . '/style.css', array(), rand(0,999), 'all');
    wp_enqueue_style('main-style'); // Enqueue it!
}
add_action( 'wp_enqueue_scripts', 'wp_bootstrap_starter_scripts' );

/** 
 * ! Temporary styles
*/
add_action('wp_enqueue_scripts', 'temp_styles'); // Add Theme Stylesheet
function temp_styles()
{
    wp_register_style('style-santiago', get_template_directory_uri() . '/style-santiago.css', array(), '1.1', 'all');
    wp_enqueue_style('style-santiago'); // Enqueue it!

    wp_register_style('style-henzly', get_template_directory_uri() . '/style-henzly.css', array(), rand(0,999), 'all');
    wp_enqueue_style('style-henzly'); // Enqueue it!

    wp_register_style('slick', get_template_directory_uri() . '/inc/assets/css/slick.css', array(), rand(0,999), 'all');
    wp_enqueue_style('slick'); // Enqueue it!
}

function club_admin_styles() {
	wp_enqueue_style( 'jquery-ui-datepicker-style' , 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css');
}
add_action('admin_print_styles', 'club_admin_styles');
function club_admin_scripts() {
	wp_enqueue_script( 'jquery-ui-datepicker' );
}
add_action('admin_enqueue_scripts', 'club_admin_scripts');


function wp_bootstrap_starter_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    <div class="d-block mb-3">' . __( "To view this protected post, enter the password below:", "wp-bootstrap-starter" ) . '</div>
    <div class="form-group form-inline"><label for="' . $label . '" class="mr-2">' . __( "Password:", "wp-bootstrap-starter" ) . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" class="form-control mr-2" /> <input type="submit" name="Submit" value="' . esc_attr__( "Submit", "wp-bootstrap-starter" ) . '" class="btn btn-primary"/></div>
    </form>';
    return $o;
}
add_filter( 'the_password_form', 'wp_bootstrap_starter_password_form' );



add_action('acf/init', 'my_acf_init');
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyDbUb6X3OuDeKLvzgqhyekKbCAs2k1Ivgk');
}


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load plugin compatibility file.
 */
require get_template_directory() . '/inc/plugin-compatibility/plugin-compatibility.php';

/**
 * Load custom WordPress nav walker.
 */
if ( ! class_exists( 'wp_bootstrap_navwalker' )) {
    require_once(get_template_directory() . '/inc/wp_bootstrap_navwalker.php');
}


add_action('um_submit_form_errors_hook_','um_custom_validate_username', 999, 1);
function um_custom_validate_username( $args ) {
    
    set_query_var( 'registeration_error', true );
}


add_action( 'um_registration_complete', 'club_registeration_success', 10, 2 );
function club_registeration_success( $user_id, $args ) {
    // message=checkmail&um_role=um_guest&um_form_id=55
    if(isset($_GET['message']) && $_GET['message'] == 'checkmail'){
        set_query_var( 'registeration_success', true );
    }
}

add_action('um_submit_form_errors_hook_','club_registration_errors_minimum_selections', 999, 1);
function club_registration_errors_minimum_selections( $args ) {
	// error_log(print_r($args,true));
	if ( !isset( $args['check_boxes_20'] ) || count( $args['check_boxes_20'] ) < 5 ) {
		UM()->form()->add_error( 'check_boxes_20', 'Please select a minimum of 5 interests' );
    }
    if ( !isset( $args['check_boxes_20_22'] ) || count( $args['check_boxes_20_22'] ) < 3 ) {
		UM()->form()->add_error( 'check_boxes_20_22', 'Please select a minimum of 3 events' );
	}
}

add_filter('the_content', 'club_showhide_registration');
function club_showhide_registration($content){
    if(!is_page('register')) return $content;

    $newcontent = '';
    $registeration_error = get_query_var( 'registeration_error', false );
    $registeration_success = (get_query_var( 'registeration_success', false ) || (isset($_GET['message']) && $_GET['message'] == 'checkmail')) ? true : false;

    if($registeration_error||$registeration_success){
        $newcontent .= '<style>.registration-join-now{display:none}.club-registration-form{display:block}</style>';
    }

    $newcontent .= $content;
    return $newcontent;
}




/********************************************************************************************************************************************************/
/**
 * Custom Settings button to trigger update the users Bios
 * https://developer.wordpress.org/reference/functions/wp_insert_post/
 */
/********************************************************************************************************************************************************/
 
 
/*
// Create an options page for one button that will trigger the update
*/
class MySettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    // private $options;
 
    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
    }
 
    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'My Update Settings', 
            'create_users', //https://codex.wordpress.org/Roles_and_Capabilities#Capabilities
            'my-setting-admin', 
            array( $this, 'create_admin_page' )
        );
    }
 
    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        // $this->options = get_option( 'my_option_name' );
 
        ?>
        <div class="wrap">
 
          <h1>My Settings</h1>
 
          <div class="box-classes">            	
            <button>Add Editions</button>
            <div class="msg"><!-- Ajax Request Here --></div>
						<div class="loader hidden"><div class="line"></div><div class="line"></div><div class="line"></div></div>
          </div><!-- box-classes -->
 
        </div><!-- wrap -->
 
				<style type="text/css">
					.loader { display:block; float:left; width:80px; height:80px; border-radius:50%; background-color:#1695A3; margin-left:16px; position:relative; box-sizing:border-box; background-color:transparent; }
					.loader.hidden { display:none; }
					.loader .line { width:30%; height:30%; border-radius:5px; background-color:#1695A3; position:absolute; bottom:50%; top:50%; transform:translateY(-50%); }
					.loader .line:first-child { animation:height1 0.9s ease-in infinite; left:0; }
					.loader .line:nth-child(2) { animation:height2 0.9s ease-in infinite; left:35%; }
					.loader .line:last-child { animation:height3 0.9s ease-out infinite; right:0; }
					@keyframes height1 {
					  0% { height:30%; }
					  50% { height:50%; }
					  100% { height:30%; }
					}
					@keyframes height2 {
					  0% { height:50%; }
					  50% { height:30%; }
					  100% { height:50%; }
					}
					@keyframes height3 {
					  0% { height:20%; }
					  50% { height:70%; }
					  100% { height:20%; }
					}	
				</style>
 
        <?php
    }
}
 
$current_user = wp_get_current_user();
if( $current_user && isset($current_user->user_login) && is_admin() ) {
 
		// Options Page
    $my_settings_page = new MySettingsPage();
	
 
    // CLASSES
		add_action( 'admin_footer', 'update_classes_action_javascript' ); // Write our JS below here
		function update_classes_action_javascript() { ?>
			<script type="text/javascript" >
			jQuery(document).ready(function($) {
 
				var data = {
					'action': 'update_classes_action',
					'whatever': 1234
				};
 
				$(".box-classes button").click(function(){
					$(".box-classes .loader").removeClass("hidden");
					// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
					jQuery.post(ajaxurl, data, function(response) {
						$(".box-classes .loader").addClass("hidden");
						if(response==""){
							$(".box-classes .msg").html("No Data");
						}else{
							var obj = JSON.parse(response);
							console.log(obj);
							$(".box-classes .msg").html("Json Data received");
						}
					});
				});
 
			});
			</script> <?php
		}
 
		add_action( 'wp_ajax_update_classes_action', 'update_classes_action' );
		function update_classes_action() {

			global $wpdb; // this is how you get access to the database
			$str = array();
			$success = 0;
			
			$args = array (
					'post_type' => 'partner',
					'posts_per_page' => -1,
					'fields' => 'ids',
					// 'p' => 2070,
			);
			$the_query = new WP_Query( $args );

			foreach ($the_query->posts as &$partnerID):

				$content = '';

				// BIO BLOCK
				$content .= '<!-- wp:tpd/block-tpd-partner-content {"partner_img_id":'.get_field( "partner_bio_image", $partnerID ).',"partner_url":"'.get_field("site",$partnerID).'","partner_social_fb":"#","partner_social_tw":"#","partner_social_in":"#"} -->';
				$content .= '<div class="wp-block-tpd-block-tpd-partner-content row" style="min-height:70vh">';
				$content .= '<div id="bio" class="col-lg-6 col-sm-6 d-flex align-items-center justify-content-center">';
				$content .= '<div class="content-partner"><div class="partner-social">';
				$content .= '<a class="social-links" href="#">';
				$content .= '<svg viewbox="0 0 64 64" width="40" height="40"><g><rect width="64" height="64" fill="#3b5998"></rect></g><g><path d="M34.1,47V33.3h4.6l0.7-5.3h-5.3v-3.4c0-1.5,0.4-2.6,2.6-2.6l2.8,0v-4.8c-0.5-0.1-2.2-0.2-4.1-0.2 c-4.1,0-6.9,2.5-6.9,7V28H24v5.3h4.6V47H34.1z" fill="#E7D6CE"></path></g></svg>';
				$content .= '</a>';
				$content .= '<a class="social-links" href="#">';
				$content .= '<svg viewbox="0 0 64 64" width="40" height="40"><g><rect width="64" height="64" fill="#00aced"></rect></g><g><path d="M48,22.1c-1.2,0.5-2.4,0.9-3.8,1c1.4-0.8,2.4-2.1,2.9-3.6c-1.3,0.8-2.7,1.3-4.2,1.6 C41.7,19.8,40,19,38.2,19c-3.6,0-6.6,2.9-6.6,6.6c0,0.5,0.1,1,0.2,1.5c-5.5-0.3-10.3-2.9-13.5-6.9c-0.6,1-0.9,2.1-0.9,3.3 c0,2.3,1.2,4.3,2.9,5.5c-1.1,0-2.1-0.3-3-0.8c0,0,0,0.1,0,0.1c0,3.2,2.3,5.8,5.3,6.4c-0.6,0.1-1.1,0.2-1.7,0.2c-0.4,0-0.8,0-1.2-0.1 c0.8,2.6,3.3,4.5,6.1,4.6c-2.2,1.8-5.1,2.8-8.2,2.8c-0.5,0-1.1,0-1.6-0.1c2.9,1.9,6.4,2.9,10.1,2.9c12.1,0,18.7-10,18.7-18.7 c0-0.3,0-0.6,0-0.8C46,24.5,47.1,23.4,48,22.1z" fill="#E7D6CE"></path></g></svg>';
				$content .= '</a>';
				$content .= '<a class="social-links" href="#">';
				$content .= '<svg viewbox="0 0 64 64" width="40" height="40"><g><rect width="64" height="64" fill="#007fb1"></rect></g><g><path d="M20.4,44h5.4V26.6h-5.4V44z M23.1,18c-1.7,0-3.1,1.4-3.1,3.1c0,1.7,1.4,3.1,3.1,3.1 c1.7,0,3.1-1.4,3.1-3.1C26.2,19.4,24.8,18,23.1,18z M39.5,26.2c-2.6,0-4.4,1.4-5.1,2.8h-0.1v-2.4h-5.2V44h5.4v-8.6 c0-2.3,0.4-4.5,3.2-4.5c2.8,0,2.8,2.6,2.8,4.6V44H46v-9.5C46,29.8,45,26.2,39.5,26.2z" fill="#E7D6CE"></path></g></svg>';
				$content .= '</a>';
				$content .= '</div>';
				$content .= '<div>';
				$content .= '<h1 class="partner_title">'. html_entity_decode(get_the_title( $partnerID )).'</h1>';
				$content .= '<button class="bio-url-link">';
				$content .= '<div class="partner_url"><a href="'.get_field('site',$partnerID).'">'.get_field('site',$partnerID).'</a></div>';
				$content .= '</button>';
				$content .= '<div class="partner_bio">'.html_entity_decode(get_field('partner_bio',$partnerID)).'</div>';
				$content .= '</div></div></div>';
				$content .= '<div id="bioimage" class="col-lg-6 col-sm-6"><img class="partner_img" src="'.get_the_post_thumbnail_url( $partnerID ).'"/></div>';
				$content .= '</div>';
				$content .= '<!-- /wp:tpd/block-tpd-partner-content -->';

				// OFFERS BLOCKS
				if( have_rows('partner_offer', $partnerID ) ):
					$content .= '<!-- wp:tpd/block-tpd-container-carousel -->
					<div class="wp-block-tpd-block-tpd-container-carousel row tpd-block-container-carousel"><div class="col-12"><div style="display:block;width:100%;height:100%"><div class="offers-carousel">
					';
					while ( have_rows('partner_offer', $partnerID ) ) : the_row();           
						$arr = array();
						$arr['title'] = get_sub_field('offer_title');
						$arr['msg'] = get_sub_field('only_members');
						$arr['note'] = get_sub_field('offer_note');
						$arr['details'] = get_sub_field('offer_details');
						$arr['image'] = get_sub_field('offer_image');
						$content .= '<!-- wp:tpd/block-tpd-carousel-item {"current_post_link":"'.get_post_permalink($partnerID).'"} -->
						<div class="wp-block-tpd-block-tpd-carousel-item item"><div class="item-cols"><div><div class="content-offer"><div><span class="title-club-offers">Club Offers</span><div class="dots-container"></div><h2 class="offer_title">'.$arr['title'].'</h2><p class="offer_notification">'.$arr['note'].'</p><span class="offer_copy">'.$arr['details'].'</span></div></div></div><div><img class="partner_logo" src="'.$arr['image']['url'].'" alt=""/></div></div></div>
						<!-- /wp:tpd/block-tpd-carousel-item -->';
					endwhile;
					$content .= '</div></div></div></div>
					<!-- /wp:tpd/block-tpd-container-carousel -->';
				endif;

				// LOCATIONS BLOCKS
				if( have_rows('partner_open_close_times', $partnerID ) ):
					$content .= '<!-- wp:tpd/block-tpd-container-locations --><div class="wp-block-tpd-block-tpd-container-locations row">';
					while ( have_rows('partner_open_close_times', $partnerID ) ) : the_row();           
						
						$arr = array();
						$arr['address'] = get_sub_field('partner_location_address');
						$arr['tube'] = get_sub_field('nearest_tube');
						$arr['phone'] = get_sub_field('partner_phone');
						$arr['email'] = get_sub_field('partner_email');
						$arr['day_of_the_week'] = get_sub_field('day_of_the_week');
						$arr['get_there_by'] = get_sub_field('get_there_by');
						// error_log( print_r($arr,true) );

						$content .= '<!-- wp:tpd/block-tpd-location-item -->';
						$content .= '<div class="wp-block-tpd-block-tpd-location-item undefined col-sm-2 col-md-4 col-lg-3">';
						$content .= '<div class="loc_address">'.$arr['address'].'</div>';
						$content .= '<div class="loc_nearest"><img src="'.esc_url(home_url()).'/wp-content/mu-plugins/tpd-partner-content/src/block-item-location/icons/tube.svg"/> <span>'.$arr['tube'].'</span></div>';
						$content .= '<div class="loc_phone"><a href="tel:'.$arr['phone'].'">'.$arr['phone'].'</a></div>';
						$content .= '<div class="loc_email"><a href="mailto:'.$arr['email'].'">'.$arr['email'].'</a></div>';
						
						$content .= '<div class="loc_how_to_get"><p>Get there by:</p>';
						$content .= ( in_array("bicycle", $arr['get_there_by']) ) ? '<img src="'.esc_url(home_url()).'/wp-content/mu-plugins/tpd-partner-content/src/block-item-location/icons/bicycle.svg"/>' : '';
						$content .= ( in_array("car", $arr['get_there_by']) ) ? '<img src="'.esc_url(home_url()).'/wp-content/mu-plugins/tpd-partner-content/src/block-item-location/icons/car.svg"/>' : '';
						$content .= ( in_array("walk", $arr['get_there_by']) ) ? '<img src="'.esc_url(home_url()).'/wp-content/mu-plugins/tpd-partner-content/src/block-item-location/icons/walk.svg"/>' : '';
						$content .= ( in_array("transit", $arr['get_there_by']) ) ? '<img src="'.esc_url(home_url()).'/wp-content/mu-plugins/tpd-partner-content/src/block-item-location/icons/public.svg"/>' : '';
						$content .= '</div>';

						$content .= '<div class="hours">';
						$content .= ( $arr['day_of_the_week'][0]['open_close_time'] ) ? '<div style="display:flex"><div style="flex-basis:100px">Monday </div><div class="loc_monday" style="flex-basis:auto">7:30am–10pm</div></div>' : '' ;
						$content .= ( $arr['day_of_the_week'][1]['open_close_time'] ) ? '<div style="display:flex"><div style="flex-basis:100px">Tuesday</div><div class="loc_tuesday" style="flex-basis:auto">7:30am–10pm</div></div>' : '' ;
						$content .= ( $arr['day_of_the_week'][2]['open_close_time'] ) ? '<div style="display:flex"><div style="flex-basis:100px">Wednesday</div><div class="loc_wednesday" style="flex-basis:auto">7:30am–10pm</div></div>' : '' ;
						$content .= ( $arr['day_of_the_week'][3]['open_close_time'] ) ? '<div style="display:flex"><div style="flex-basis:100px">Thursday</div><div class="loc_thursday" style="flex-basis:auto">7:30am–10pm</div></div>' : '' ;
						$content .= ( $arr['day_of_the_week'][4]['open_close_time'] ) ? '<div style="display:flex"><div style="flex-basis:100px">Friday</div><div class="loc_friday" style="flex-basis:auto">7:30am–10pm</div></div>' : '' ;
						$content .= ( $arr['day_of_the_week'][5]['open_close_time'] ) ? '<div style="display:flex"><div style="flex-basis:100px">Saturday</div><div class="loc_saturday" style="flex-basis:auto">Closed</div></div>' : '' ;
						$content .= ( $arr['day_of_the_week'][6]['open_close_time'] ) ? '<div style="display:flex"><div style="flex-basis:100px">Sunday</div><div class="loc_sunday" style="flex-basis:auto">Closed</div></div></div>' : '' ;
						$content .= '</div>';
						$content .= '<!-- /wp:tpd/block-tpd-location-item -->';

					endwhile;
					$content .= '</div><!-- /wp:tpd/block-tpd-container-locations -->';
				endif;

				// update post meta 'partner_bio_logo' to contain the logo's ID
				update_post_meta( $partnerID, 'partner_bio_logo', get_field( "partner_bio_image", $partnerID ) );

				// update content for the partner
				$my_post = array(
						'ID'           => $partnerID,
						'post_content' => $content,
				);
				$result = wp_update_post( $my_post );
				if( $result > 0 )
						$success ++;

			endforeach;
    
			$str[] = array( "Rows: ".count($the_query->posts), "Failed: ".(count($the_query->posts)-$success), "Success: ".$success );
			echo json_encode($str);
			wp_die();
 
		}//update_classes_action


}
add_action( 'um_after_login_fields', 'my_after_form_fields', 10, 1 );
function my_after_form_fields( $args ) {
    // error_log(print_r($args,true));
}

/**
 * Show the submit button
 *
 * @param $args
 */
function cityam_add_submit_button_to_login( $args ) {
	/**
	 * UM hook
	 *
	 * @type filter
	 * @title um_login_form_button_one
	 * @description Change Login Form Primary button
	 * @input_vars
	 * [{"var":"$primary_btn_word","type":"string","desc":"Button text"},
	 * {"var":"$args","type":"array","desc":"Login Form arguments"}]
	 * @change_log
	 * ["Since: 2.0"]
	 * @usage
	 * <?php add_filter( 'um_login_form_button_one', 'function_name', 10, 2 ); ?>
	 * @example
	 * <?php
	 * add_filter( 'um_login_form_button_one', 'my_login_form_button_one', 10, 2 );
	 * function my_login_form_button_one( $primary_btn_word, $args ) {
	 *     // your code here
	 *     return $primary_btn_word;
	 * }
	 * ?>
	 */
	$primary_btn_word = apply_filters('um_login_form_button_one', $args['primary_btn_word'], $args );

	if ( ! isset( $primary_btn_word ) || $primary_btn_word == '' ){
		$primary_btn_word = UM()->options()->get( 'login_primary_btn_word' );
	}

	/**
	 * UM hook
	 *
	 * @type filter
	 * @title um_login_form_button_two
	 * @description Change Login Form Secondary button
	 * @input_vars
	 * [{"var":"$secondary_btn_word","type":"string","desc":"Button text"},
	 * {"var":"$args","type":"array","desc":"Login Form arguments"}]
	 * @change_log
	 * ["Since: 2.0"]
	 * @usage
	 * <?php add_filter( 'um_login_form_button_two', 'function_name', 10, 2 ); ?>
	 * @example
	 * <?php
	 * add_filter( 'um_login_form_button_two', 'my_login_form_button_two', 10, 2 );
	 * function my_login_form_button_two( $secondary_btn_word, $args ) {
	 *     // your code here
	 *     return $secondary_btn_word;
	 * }
	 * ?>
	 */
	$secondary_btn_word = apply_filters( 'um_login_form_button_two', $args['secondary_btn_word'], $args );

	if ( ! isset( $secondary_btn_word ) || $secondary_btn_word == '' ){
		$secondary_btn_word = UM()->options()->get( 'login_secondary_btn_word' );
	}

	$secondary_btn_url = ! empty( $args['secondary_btn_url'] ) ? $args['secondary_btn_url'] : um_get_core_page( 'register' );
	/**
	 * UM hook
	 *
	 * @type filter
	 * @title um_login_form_button_two_url
	 * @description Change Login Form Secondary button URL
	 * @input_vars
	 * [{"var":"$secondary_btn_url","type":"string","desc":"Button URL"},
	 * {"var":"$args","type":"array","desc":"Login Form arguments"}]
	 * @change_log
	 * ["Since: 2.0"]
	 * @usage
	 * <?php add_filter( 'um_login_form_button_two_url', 'function_name', 10, 2 ); ?>
	 * @example
	 * <?php
	 * add_filter( 'um_login_form_button_two_url', 'my_login_form_button_two_url', 10, 2 );
	 * function my_login_form_button_two_url( $secondary_btn_url, $args ) {
	 *     // your code here
	 *     return $secondary_btn_url;
	 * }
	 * ?>
	 */
	$secondary_btn_url = apply_filters( 'um_login_form_button_two_url', $secondary_btn_url, $args ); ?>

	<div class="um-col-alt">

		<?php if ( isset( $args['show_rememberme'] ) && $args['show_rememberme'] ) {
			UM()->fields()->checkbox( 'rememberme', __( 'Keep me signed in', 'ultimate-member' ), false ); ?>
			<div class="um-clear"></div>
		<?php }

		if ( isset( $args['secondary_btn'] ) && $args['secondary_btn'] != 0 ) { ?>

			<div class="um-left um-half">
				<input type="submit" value="<?php esc_attr_e( wp_unslash( $primary_btn_word ), 'ultimate-member' ); ?>" class="um-button" id="um-submit-btn" />
			</div>
			<div class="um-right um-half">
				<a href="<?php echo esc_url( $secondary_btn_url ); ?>" class="um-button um-alt">
					<?php _e( wp_unslash( $secondary_btn_word ), 'ultimate-member' ); ?>
				</a>
			</div>

		<?php } else { ?>

			<div class="um-center">
				<button type="submit" value="<?php esc_attr_e( wp_unslash( $primary_btn_word ), 'ultimate-member' ); ?>" class="um-button" id="um-submit-btn" /><?php esc_attr_e( wp_unslash( $primary_btn_word ), 'ultimate-member' ); ?></button>
			</div>

		<?php } ?>

		<div class="um-clear"></div>

	</div>

	<?php
}

/**
 * Display a forgot password link
 *
 * @param $args
 */
function club_after_login_submit( $args ) {
	if ( $args['forgot_pass_link'] == 0 ) {
		return;
	} ?>

	<div class="um-col-alt-b">
		<a href="<?php echo esc_url( um_get_core_page( 'password-reset' ) ); ?>" class="um-link-alt">
			<?php _e( 'Forgot your password?', 'ultimate-member' ); ?>
		</a>
	</div>

	<?php
}
remove_action( 'um_after_login_fields', 'um_add_submit_button_to_login', 1000 );
remove_action( 'um_after_login_fields', 'um_after_login_submit', 1001 );
add_action( 'um_after_login_fields', 'club_after_login_submit', 1000 );
add_action( 'um_after_login_fields', 'cityam_add_submit_button_to_login', 1001 );


/**
 * Url:https://stackoverflow.com/questions/49742442/remove-phone-from-billing-shipping-fields-everywhere-in-woocommerce
 * 
 */
// Remove billing phone (and set email field class to wide)
add_filter( 'woocommerce_billing_fields', 'remove_billing_phone_field', 20, 1 );
function remove_billing_phone_field($fields) {
    $fields ['billing_phone']['required'] = false; // To be sure "NOT required"

    $fields['billing_email']['class'] = array('form-row-wide'); // Make the field wide

    unset( $fields ['billing_phone'] ); // Remove billing phone field
    return $fields;
}

// Remove shipping phone (optional)
add_filter( 'woocommerce_shipping_fields', 'remove_shipping_phone_field', 20, 1 );
function remove_shipping_phone_field($fields) {
    $fields ['shipping_phone']['required'] = false; // To be sure "NOT required"

    unset( $fields ['shipping_phone'] ); // Remove shipping phone field
    return $fields;
}

function club_get_site_logo()
{
    if(has_custom_logo()):
        the_custom_logo();
    else: ?>
		<a href=<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<img class="custom-logo" src="<?php echo (wp_is_mobile() || !is_front_page()) ? get_template_directory_uri() . '/assets/svg/text_logo-alt.svg' : get_template_directory_uri() . '/assets/svg/full_logo-alt.svg'; ?>" alt="" srcset="">
		</a>
    <?php endif;
}

function club_loginregister_nav()
{
	return wp_nav_menu(array(
		'theme_location'    => 'register',
		'container'       => 'div',
		'container_id'    => 'register-nav',
		'container_class' => '',
		'menu_id'         => false,
		'menu_class'      => 'navbar-nav',
		'depth'           => 3,
		'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
		'walker'          => new wp_bootstrap_navwalker()
	));
}

/**
 * * Add guest_role class if that is the only role
 */
add_filter( 'body_class', 'add_guest_role_to_body_class' );
function add_guest_role_to_body_class( $classes ) {
	$user = wp_get_current_user();
	if ( isset($user->roles) && count($user->roles)==1 && $user->roles[0] == 'um_guest' ) {
		$classes[] = sanitize_html_class( 'guest_only' );
	}
	return $classes;
}

/**
*  @param $user
*  @author Webkul
* Url: https://webkul.com/blog/wordpress-custom-fields-user-profile-page/
*/
 
add_action( 'edit_user_profile', 'wk_custom_user_profile_fields' );
add_action( 'show_user_profile', 'wk_custom_user_profile_fields' );

function wk_custom_user_profile_fields( $user )
{
    echo '<h3 class="heading">Additional Info</h3>';
	
	$interests = array();
	$interests_events = array();

	$terms = get_terms( array(
		'taxonomy' => array('tribe_events_cat', 'category'),
		'hide_empty' => false,
	) );

	foreach ($terms as $key => $term) {
		if($term->taxonomy == 'category'){
			$interests[$term->term_id] = $term->name;
		}
		if($term->taxonomy == 'tribe_events_cat'){
			$interests_events[$term->term_id] = $term->name;
		}
		
	}
	

	$fields = array(
		array(
			'label' => 'Birth Date',
			'name' => 'birth_date',
			'type' => 'text',
			'value' => get_user_meta( $user->ID, 'birth_date', true ) ? date( 'j M Y', strtotime(get_user_meta( $user->ID, 'birth_date', true ))) : ''
		),
		array(
			'label' => 'Gender',
			'name' => 'Gender',
			'type' => 'select',
			'value' => get_user_meta( $user->ID, 'Gender', true ) ? get_user_meta( $user->ID, 'Gender', true ) : '',
			'options' => array(
				'Male',
				'Female',
				'Prefer Not to Say'
			)
		),
		array(
			'label' => 'Job Title',
			'name' => 'job_title',
			'type' => 'text',
			'value' => get_user_meta( $user->ID, 'job_title', true ) ? get_user_meta( $user->ID, 'job_title', true ) : '',
		),
		array(
			'label' => 'Industry',
			'name' => 'industry',
			'type' => 'select',
			'value' => get_user_meta( $user->ID, 'industry', true ) ? get_user_meta( $user->ID, 'industry', true ) : '',
			'options' => array(
				'Accounting',
				'Arts and Entertainment',
				'Banking',
				'Brokerage & Investment',
				'Business and Consulting',
				'Charity and Volunteer Work',
				'Creative Arts and Design',
				'Education',
				'Engineering',
				'Environment and Agriculture',
				'Event Planning',
				'Food Services',
				'Hospitality',
				'Information Technology',
				'Legal Services',
				'Leisure, Sport and Tourism',
				'Marketing, Advertising, and PR',
				'Media',
				'Property/Real Estate',
				'Public Services and Administration',
				'Recruitment and HR',
				'Retail',
				'Science and Pharmaceuticals',
				'Transport and Logistics',
				'Other'
			)
		),
		array(
			'label' => 'Salary',
			'name' => 'salary',
			'type' => 'select',
			'value' => get_user_meta( $user->ID, 'salary', true ) ? get_user_meta( $user->ID, 'salary', true ) : '',
			'options' => array(
				'< £35k',
				'£35k to £50k',
				'£50k to £75k',
				'£75k to £100k',
				'£100k to £150k',
				'£150k to £250k',
				'£250k +',
				'Prefer not to say'
			)
		),
		array(
			'label' => 'Interests',
			'name' => 'interest',
			'type' => 'checkbox',
			'value' => get_user_meta( $user->ID, 'interest', true ) ? get_user_meta( $user->ID, 'interest', true ) : array(),
			'options' => $interests
		),
		array(
			'label' => 'Event Interests',
			'name' => 'event_interest',
			'type' => 'checkbox',
			'value' => get_user_meta( $user->ID, 'event_interest', true ) ? get_user_meta( $user->ID, 'event_interest', true ) : array(),
			'options' => $interests_events
		),
	)
    ?>
    
    <table class="form-table">

	<?php foreach ($fields as $key => $field) : ?>
	<tr>
        <th><label for="<?php echo $field['name']; ?>"><?php echo $field['label']; ?></label></th>
 
		<?php if( $field['type'] == 'select' ) : ?>
			<td>
				<select id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>">
				<option value="">Make a selection</option>
				<?php foreach( $field['options'] as $k => $option ) : ?>
					<?php if ( $option == $field['value'] ) : ?>
						<option selected value="<?php echo esc_attr( $option ); ?>"><?php echo $option; ?></option>
					<?php else: ?>
						<option value="<?php echo esc_attr( $option ); ?>"><?php echo $option; ?></option>
					<?php endif; ?>
					
				<?php endforeach; ?>
				</select>
			</td>
		
		<?php elseif( $field['type'] == 'checkbox' ) : ?>
			<td>
			<?php foreach( $field['options'] as $k => $option ) : ?>
				<?php if ( in_array( $k, $field['value'] ) ) : ?>
					<div>
					<input checked type="<?php echo $field['type']; ?>" id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>[]" value="<?php echo esc_attr( $k ); ?>">
					<label for="<?php echo $field['name']; ?>"><?php echo $option; ?></label>
					</div>
				<?php else: ?>
					<div>
					<input type="<?php echo $field['type']; ?>" id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>[]" value="<?php echo esc_attr( $k ); ?>">
					<label for="<?php echo $field['name']; ?>"><?php echo $option; ?></label>
					</div>
				<?php endif; ?>
				
			<?php endforeach; ?>
			</td>

		<?php else: ?>
		<td>
			<input type="<?php echo $field['type']; ?>" id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>" value="<?php echo esc_attr( $field['value'] ); ?>">
		</td>
		<?php endif; ?>

 
	</tr>
	<?php endforeach; ?>


    </table>
    <script>
    jQuery(document).ready(function($) {
        $("#birth_date").datepicker({
			dateFormat: "d M yy",
			changeMonth: true,
			changeYear: true
		});
    });
	</script>
    <?php
}

add_action( 'edit_user_profile_update', 'wk_save_custom_user_profile_fields', 11 );
add_action('personal_options_update', 'wk_save_custom_user_profile_fields', 11); 
/**
*   @param User Id $user_id
*/
function wk_save_custom_user_profile_fields( $user_id )
{

	$fields = array(
		array(
			'label' => 'Birth Date',
			'name' => 'birth_date',
		),
		array(
			'label' => 'Gender',
			'name' => 'Gender',
		),
		array(
			'label' => 'Job Title',
			'name' => 'job_title',
		),
		array(
			'label' => 'Industry',
			'name' => 'industry',
		),
		array(
			'label' => 'Salary',
			'name' => 'salary',
		),
		array(
			'name' => 'interest',
		),
		array(
			'name' => 'event_interest',
		),
	);

	$terms = [];
	foreach ($fields as $key => $field){
		if($field['name'] == 'birth_date'){
			$_POST[$field['name']] = date( 'Y/m/d', strtotime(  $_POST[$field['name']] ) );
		}
		if($field['name'] == 'interest'){
			if( isset($_POST[$field['name']]) && count($_POST[$field['name']]) > 0){
				foreach ($_POST[$field['name']] as $key => $value) {
					// add condition to check if object or array has values first.
					$term = get_term_by('id', $value, 'category');
					$terms[] = $term->name;
				}
			}
			update_user_meta( $user_id, 'check_boxes_20', $terms );
		}
		if($field['name'] == 'event_interest'){
			if(isset($_POST[$field['name']]) && count($_POST[$field['name']]) > 0){
				foreach ($_POST[$field['name']] as $key => $value) {
					$term = get_term_by('id', $value, 'tribe_events_cat');
					$terms[] = $term->name;
				}
			}
			update_user_meta( $user_id, 'check_boxes_20_22', $terms );
		}
		update_user_meta( $user_id, $field['name'], isset($_POST[$field['name']]) ? $_POST[$field['name']] : '' );
	}
    
 
}

add_action('um_after_account_general', 'showUMExtraFields', 10);

function showUMExtraFields() {
  $id = um_user('ID');
  $output = '';
  $names = array(
	  'birth_date', 
	  'Gender', 
	  'first_line_of_address', 
	  'town_city', 
	  'postcode', 
	  'your_job_title',
	  'name_of_company', 
	  'industry', 
	  'salary', 
	  'check_boxes_20', 
	  'check_boxes_20_22'
);

  $fields = array(); 
  foreach( $names as $name ){
	$fields[ $name ] = UM()->builtin()->get_specific_field( $name );
	if(!isset($fields[ $name ]['label'])){
		$fields[ $name ]['label'] = $fields[ $name ]['title'];
	}
  }

  
  $fields = apply_filters('um_account_secure_fields', $fields, $id);

  foreach( $fields as $key => $data ){
	
	if(!isset($data['label'])){
		$data['label'] = $data['title'];
	}

    $output .= UM()->fields()->edit_field( $key, $data );
  }


  echo $output;
  
}

add_action('um_account_pre_update_profile', 'getUMFormData', 10);

function getUMFormData(){

	$post_tags = get_terms( array(
		'taxonomy' => array('tribe_events_cat', 'category'),
		'hide_empty' => false,
	) );

	$interests = array();
	$event_interests = array();
	$id = um_user('ID');
	$names = array(
		'birth_date', 
		'Gender', 
		'first_line_of_address', 
		'town_city', 
		'postcode', 
		'your_job_title',
		'name_of_company', 
		'industry', 
		'salary', 
		'check_boxes_20', 
		'check_boxes_20_22'
	);

	foreach ($names as $key => $name) {
		if($name == 'birth_date' && isset($_POST[$name]) ){
			$_POST[$name] = date( 'Y/m/d', strtotime( $_POST[$name] ) );
		}

		if($name == 'check_boxes_20' && isset($_POST[$name])){
			foreach ($post_tags as $key => $tag) {
				if(in_array($tag->name, $_POST[$name])){
					$interests[] = $tag->term_id;
				}
			}
			//update_user_meta( $id, $name, $_POST[$name] );
			update_user_meta( $id, 'interest', $interests );
		}

		if($name == 'check_boxes_20_22' && isset($_POST[$name])){
			foreach ($post_tags as $key => $tag) {
				if(in_array($tag->name, $_POST[$name])){
					$event_interests[] = $tag->term_id;
				}
			}
			// update_user_meta( $id, $name, $_POST[$name] );
			update_user_meta( $id, 'event_interest', $event_interests );
		}

		if(isset($_POST[$name])){
			update_user_meta( $id, $name, $_POST[$name] );
		}
	}

}

function redirect_login_page(){

	// Store for checking if this page equals wp-login.php
	$page_viewed = basename($_SERVER['REQUEST_URI']);
   
	// Where we want them to go
	$login_page  = '/login'; //Write your site URL here.
	// Two things happen here, we make sure we are on the login page
	// and we also make sure that the request isn't coming from a form
	// this ensures that our scripts & users can still log in and out.
	if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
   
		// And away they go...
		wp_redirect($login_page);
		exit();
   
	}

	if( $page_viewed == "wp-login" ) {
   
		// And away they go...
		wp_redirect($login_page);
		exit();
	}
   
}

add_action('init','redirect_login_page');

add_action( 'woocommerce_order_status_completed', 'club_change_role_on_purchase' );

function club_change_role_on_purchase( $order_id ) {

// get order object and items
	$order = new WC_Order( $order_id );
	$items = $order->get_items();

	$product_membershp_yearly = 3013; // that's a specific product ID
	$product_membershp_monthly = 3014; // that's a specific product ID

	foreach ( $items as $item ) {

		if( ( $product_membershp_yearly == $item['product_id'] || $product_membershp_monthly == $item['product_id'] ) && $order->user_id ) {

			$user = new WP_User( $order->user_id );

			if ( in_array( 'um_guest', $user->roles ) ){
				// Remove old role
				$user->remove_role( 'um_guest' ); 

				// Add new role
				// $user->add_role( 'editor' );
			}
		}
	}
}