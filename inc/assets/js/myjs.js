(function( root, $, undefined ) {
	"use strict";

	$(function () {

    
        // run the currently selected effect
        function runEffect() {
            // get effect type from
            var selectedEffect = $("#effectTypes").val();
            // Most effect types need no options passed by default
            var options = {};
            // some effects have required parameters
            if (selectedEffect === "scale") {
                options = {
                    percent: 50
                };
            } else if (selectedEffect === "size") {
                options = {
                    to: {
                        width: 280,
                        height: 185
                    }
                };
            }
            // Run the effect
            $("#effect").show(selectedEffect, options, 500, callback);
        };
        //callback function to bring a hidden box back
        function callback() {
            setTimeout(function() {
                $("#effect:visible").removeAttr("style").fadeOut();
            }, 1000);
        };
        // Set effect from select menu value
        $("#button").on("click", function() {
            runEffect();
        });
        $("#effect").hide();

        function switchVisible() {
            if (document.getElementById('Div1')) {
                if (document.getElementById('Div1').style.display == 'none') {
                    document.getElementById('Div1').style.display = 'block';
                    document.getElementById('Div2').style.display = 'none';
                } else {
                    document.getElementById('Div1').style.display = 'none';
                    document.getElementById('Div2').style.display = 'block';
                }
            }
        }
        // jQuery alternative for above switchVisible func
        $("div#Div1 input#Button1,.registration-join-now .wp-block-button__link").click(function(){
            $("div#Div1,.registration-join-now").hide();
            $("div#Div2,.club-registration-form").show();
            $('body.page-template-page-register main > .container-fluid > .wp-block-columns > .wp-block-column:nth-child(2)').css('justify-content','flex-start');
        });

        $(".registration-join-now .wp-block-button__link").click(function(){
            $(".join-help-text h3").text("Next");
            $(".join-help-text h1").text("Tell us about yourself");
            $(".join-help-text h1 + p").text("Let's start building your account");
        });        


        $('.fancybox').fancybox({
            toolbar: false,
            smallBtn: true,
            iframe: {
                preload: false
            }
        });

        /*
        *  new_map
        *
        *  This function will render a Google Map onto the selected jQuery element
        *
        *  @type	function
        *  @date	8/11/2013
        *  @since	4.3.0
        *
        *  @param	$el (jQuery element)
        *  @return	n/a
        */
        function new_map($el) {
            // var
            var $markers = $el.find('.marker');
            // vars
            var args = {
                zoom: 16,
                center: new google.maps.LatLng(0, 0),
                styles: [
                    {
                        "featureType": "landscape",
                        "stylers": [
                            {"hue": "#FFBB00"},
                            {"saturation": 43.400000000000006},
                            {"lightness": 37.599999999999994},
                            {"gamma": 1}
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "stylers": [
                            {"hue": "#FFC200"},
                            {"saturation": -61.8},
                            {"lightness": 45.599999999999994},
                            {"gamma": 1}
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "stylers": [
                            {"hue": "#FF0300"},
                            {"saturation": -100},
                            {"lightness": 51.19999999999999},
                            {"gamma": 1}
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "stylers": [
                            {"hue": "#FF0300"},
                            {"saturation": -100},
                            {"lightness": 52},
                            {"gamma": 1}
                        ]
                    },
                    {
                        "featureType": "water",
                        "stylers": [
                            {"hue": "#0078FF"},
                            {"saturation": -13.200000000000003},
                            {"lightness": 2.4000000000000057},
                            {"gamma": 1}
                        ]
                    },
                    {
                        "featureType": "poi",
                        "stylers": [
                            {"hue": "#00FF6A"},
                            {"saturation": -1.0989010989011234},
                            {"lightness": 11.200000000000017},
                            {"gamma": 1}
                        ]
                    },
                    {
                        "featureType": "poi.business",
                        "stylers": [
                          {
                            "visibility": "off"
                          }
                        ]
                      },
                ],
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            // create map	        	
            var map = new google.maps.Map($el[0], args);
            // add a markers reference
            map.markers = [];
            // add markers
            $markers.each(function() {
                add_marker($(this), map);
            });
            // center map
            center_map(map);
            // return
            return map;
        }
        /*
        *  add_marker
        *
        *  This function will add a marker to the selected Google Map
        *
        *  @type	function
        *  @date	8/11/2013
        *  @since	4.3.0
        *
        *  @param	$marker (jQuery element)
        *  @param	map (Google Map object)
        *  @return	n/a
        */
        function add_marker($marker, map) {
            // var
            var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));
            // create marker
            var marker = new google.maps.Marker({
                position: latlng,
                map: map
            });
            // add to array
            map.markers.push(marker);
            // if marker contains HTML, add it to an infoWindow
            if ($marker.html()) {
                // create info window
                var infowindow = new google.maps.InfoWindow({
                    content: $marker.html()
                });
                // show info window when marker is clicked
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker);
                });
            }
        }
        /*
        *  center_map
        *
        *  This function will center the map, showing all markers attached to this map
        *
        *  @type	function
        *  @date	8/11/2013
        *  @since	4.3.0
        *
        *  @param	map (Google Map object)
        *  @return	n/a
        */
        function center_map(map) {
            // vars
            var bounds = new google.maps.LatLngBounds();
            // loop through all markers and create bounds
            $.each(map.markers, function(i, marker) {
                var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
                bounds.extend(latlng);
            });
            // only 1 marker?
            if (map.markers.length == 1) {
                // set center of map
                map.setCenter(bounds.getCenter());
                map.setZoom(16);
            } else {
                // fit to bounds
                map.fitBounds(bounds);
            }
        }
        /*
        *  document ready
        *
        *  This function will render each map when the document is ready (page has loaded)
        *
        *  @type	function
        *  @date	8/11/2013
        *  @since	5.0.0
        *
        *  @param	n/a
        *  @return	n/a
        */
        // global var
        var map = null;
        $(document).ready(function() {
            $('.acf-map').each(function() {
                // create map
                map = new_map($(this));
            });

            $('.happening-now-carousel').slick({
                speed: 6000,
                autoplay: true,
                autoplaySpeed: 0,
                centerMode: true,
                cssEase: 'linear',
                slidesToShow: 1,
                slidesToScroll: 1,
                variableWidth: true,
                infinite: true,
                arrows:false,
            });

            $('.logo-showcase-carousel').slick({
                initialSlide:1,
                speed: 4000,
                autoplay: true,
                autoplaySpeed: 0,
                centerMode: true,
                cssEase: 'linear',
                slidesToShow: 1,
                slidesToScroll: 1,
                variableWidth: true,
                infinite: true,
                arrows:false,
            });

        });
        $(document).ready(function() {
            $('.togglebutton').on('click', function() {
                // do something...

                if($(this).hasClass('collapsed')){
                    $(this).parent().css({
                    'border-bottom-right-radius': '0px'
                    });
                }else{
                    $(this).parent().css({
                    'border-bottom-right-radius': '10px'
                    });
                }

            });
            
            if($('#tribe-bar-form input[name=tribe-bar-date]').val() != ""){
                console.log($(this).val());
                $('#tribe-bar-form input[name=tribe-bar-date]').parent().find('label').addClass('input_has_value');
            }else{
                $('#tribe-bar-form input[name=tribe-bar-date]').parent().find('label').removeClass('input_has_value');
            }

            if($('#tribe-bar-form input[name=tribe-bar-search]').val() != ""){
                console.log($(this).val());
                $('#tribe-bar-form input[name=tribe-bar-search]').parent().find('label').addClass('input_has_value');
            }else{
                $('#tribe-bar-form input[name=tribe-bar-search]').parent().find('label').removeClass('input_has_value');
            }

            $('#tribe-bar-form input[type=text]').on('change load',function(e){
                
                if($(this).val() != ""){
                    $(this).parent().find('label').addClass('input_has_value');
                }else{
                    $(this).parent().find('label').removeClass('input_has_value');
                }
            });



        });

        $(document).ready(function() {
            $('.hamburger').on('click', function(e) {
                $('header.site-header').toggleClass('show')
            });
        });

	});

} ( this, jQuery ));
