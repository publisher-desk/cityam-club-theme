<?php
/**
 * Template Name: Register
 */
get_header();
$registeration_error = get_query_var( 'registeration_error', false );
$registeration_success = (get_query_var( 'registeration_success', false ) || (isset($_GET['message']) && $_GET['message'] == 'checkmail')) ? true : false;
?>

<main>
	<div class="container-fluid">
		<?php
			while ( have_posts() ) : the_post();

				the_content();

			endwhile; // End of the loop.
			?>
	</div>
</main>

<?php
get_footer();
