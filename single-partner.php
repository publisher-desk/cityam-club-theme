<?php get_header(); ?>

	<main id="main" class="site-main" role="main">
 		<div id="content" class="container-fluid site-content">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<p><?php the_content(); ?></p>
			<?php endwhile; endif; ?>

		</div>

	</main><!-- #main -->

<?php get_footer(); ?>
