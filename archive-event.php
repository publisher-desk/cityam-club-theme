<?php
/**
 * The template for displaying archive event
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
	<div id="content" class="container-fluid site-content">
		<div class="container">
			<div id="filter">
				<div class="row">
					<div class="col-md-5">
						<h1>Upcoming events</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<p>The City A.M Club is your passport to the City, offering you exclusive access to London’s most enticing and exciting experiences.</p>
					</div>			
					
					<span class="postnumber">
						<?php
							// Get total number of posts in POST-TYPE-NAME
							$count_posts = wp_count_posts('event');
							$total_posts = $count_posts->publish;
							echo $total_posts . ' Events ';;
						?>
					</span>
				</div>
			</div>
		</div>
	</div>



<div id="partnerpage" class="container-fluid">
<section id="personalfeed">
<div class="container">

<?php 
$args = array(
	'post_type' => array('event')
);
$query = new WP_Query( $args );
?>
<div class="row">
<div class="card-columns">
<?php 

// the query
$the_query = new WP_Query( $args ); ?>

<?php if ( $the_query->have_posts() ) : ?>

	<!-- pagination here -->

	<!-- the loop -->
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    <div class="card">
	<a href="<?php the_permalink(); ?>">
	<h2 class="archivefeed"><?php the_title(); ?></h2>
	<!-- start date/time -->
		<div>
		<?php 
		if (get_field('start_date')) {
			?>
				<p class="eventdate" style="float:left;"><?php the_field('start_date'); ?> </p>
				<?php 
		} // end if value
		?>
		</div>
 
       
		</div>
	<?php endwhile; ?>
	<!-- end of the loop -->


	<?php wp_reset_postdata(); ?>
        </div>
<?php else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
	</div>
</div>
</section><!-- #primary -->

    </div>


<?php
get_footer();


