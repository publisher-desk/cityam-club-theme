<?php
/**
 * Template Name: Account
 */

get_header();
?>	 
	<div id="content" class="container-fluid site-content accountpage">
<div class="container-fluid">
		
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php
		while ( have_posts() ) : the_post();
		the_content();
		endwhile; // End of the loop.

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-bootstrap-starter' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
			</div>
         </div>
 

<?php
get_footer();
