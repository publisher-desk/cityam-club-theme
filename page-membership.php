<?php get_header(); ?>

<section id="primary" class="col">
  <main id="main" class="site-main" role="main">

    <article id="post-<?php the_ID(); ?>" <?php post_class('membership'); ?>>
      <div class="entry-content">
    
        <?php $user = wp_get_current_user(); ?>
        <?php if ( is_user_logged_in() && in_array('um_guest',$user->roles) && !in_array('subscriber',$user->roles) ): ?>
          <?php while ( have_posts() ) : the_post(); ?>
            <div class="cols">
              <div>
                <h3>Last step</h3>
                <h1>Become a member</h1>
                <p>Anual membership: 240 (incl VAT)</p>
                <p>Monthly membership: 24 (incl VAT)</p>
                <p>Your anual membership will begin today 2.3.2020, and expires 1.3.2021. You can cancel your subscription at any time in your account, within 2 weeks notice.</p>
              </div>
              <div>
                <?php the_content(); ?>
              </div>
            </div>
            
          <?php endwhile; ?>
        <?php elseif ( is_user_logged_in() && in_array('um_guest',$user->roles) && in_array('subscriber',$user->roles) ): ?>
          <div class="cols">
            <div>
              <h3>Welcome</h3><br>
              <?php $subs = wcs_get_users_subscriptions(); ?>
              <?php $order = wc_get_order( key($subs) ); ?>
              <?php $order_t = $order->get_date('end'); ?>
              <?php $expiration_date = explode(" ", $order_t, 2)[0]; ?>
              <?php if ( wcs_user_has_subscription() ) : ?>
                <p>Card expires on: <?php echo $expiration_date; ?></p>
              <?php endif; ?>
              <p>You can cancel your subscription at any time in your account, within 2 weeks notice.</p>
            </div>
            <div>
              <?php echo wp_get_attachment_image( get_the_ID(), 'thumbnail', "", array( "class" => "img-fluid" ) ); ?>
            </div>
          </div>
        <?php else: ?>
          <div class="cols">
              <div>
                <h1>Join the Club now!</h1>
                <p>Since 1849 fourteen cast-iron dragons have guarded every major entrance into the Square Mile. Each dragon statue is seven feet high, marking the boundaries of The City of London. These were also the first distribution points for City AM newspaper at launch in 2005 and we have been there every day since. So now the dragons, as well as guarding The City of London, guard entry into the City AM Club!</p>
              </div>
              <div>
                <?php echo wp_get_attachment_image( get_the_ID(), 'thumbnail', "", array( "class" => "img-fluid" ) ); ?>
              </div>
            </div>
        
        <?php endif; ?>
      
      </div>
    </article>

  </main><!-- #main -->
</section><!-- #primary -->

<?php
get_footer();
