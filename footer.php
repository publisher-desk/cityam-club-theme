			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
	
	<div class="container-fluid">
		<footer class="site-footer" role="contentinfo">
			<div class="row">		
				<div class="col-sm-12 col-md-12" style="margin: 0 auto;">
					<div class="footerimage"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2" style="margin: 0 auto;">
					<ul id="footernav">
						<li><a href="<?php echo esc_url( home_url() ); ?>/privacy-policy/" target="_blank;">Privacy policy</a></li>
							<br> 
						<li><a href="<?php echo esc_url( home_url() ); ?>/terms-of-service/" target="_blank;">Terms of Service</a></li> <br>
						<li><a href="<?php echo esc_url( home_url() ); ?>/partner/">View all partners</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<?php get_template_part( 'template-parts/footer-social', 'links' ); ?>
			</div>
		</footer>
	</div>

</div><!-- #page -->
		
<?php /* https://css-tricks.com/need-to-scroll-to-the-top-of-the-page/ */ ?>
<a href="#top" onclick="window.scroll({ top: 0, left: 0, behavior: 'smooth'});" style="display:none;z-index:10;">
	<img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/up_arrow.png" alt="back to top">
</a>

<?php wp_footer(); ?>
</body>
</html>
