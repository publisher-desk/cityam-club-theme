<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>
<section id="personalfeed">
    <div class="container">
        <div class="row">
            <div class="personalfeed-intro">
                <h2>Personalised Feed</h2>
                <p>All City AM Club Members activate a personalised feed, tailored to your individual preferences
                    upon joining. Club Members also receive a weekly email of upcoming events, as well as active
                    City AM Club social media feeds.<br>
                    <br>
                </p>
            </div>
            <div class="feed-columns">
                <?php 
			$args = array(
				'post_type' => array('partner','tribe_events'),
				'orderby'   => 'rand',
				'posts_per_page' => 6,
			);

			if(is_user_logged_in()){
				$user_id = get_current_user_id();
				$interests = get_field('interest','user_'.$user_id);
				foreach ($interests as $key => $interest) {
                    if(!is_object($interest)){
                        $interest = get_term_by( 'term_taxonomy_id', $interest );
                    }
                    
					$tax[] = array(
						'taxonomy' => $interest->taxonomy,
						'field'    => 'term_id',
						'terms'    => array($interest->term_id),
					);
					
				}
                $args['tax_query'] = $tax;
                $args['tax_query']['relation'] = 'OR';
            }
			$query = new WP_Query( $args );
			?>
                <?php // the query 
		$the_query = new WP_Query( $args ); ?>

                <?php if ( $the_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="feed col-sm-6 col-lg-4">
                    <a href="<?php the_permalink(); ?>">
                        <div class="overlay">
                            <?php the_post_thumbnail(); ?>
                        </div>

                        <div class="featured-image-container"
                            style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>);">
                            <?php //the_post_thumbnail( 'medium' ); ?>
                        </div>

                        <!-- start date/time -->
                        <div class="feed-headline">
                            <h2><?php the_title(); ?></h2>
                            <?php if (get_field('start_date')) : ?>
                            <p class="date"><?php the_field('start_date'); ?></p>
				            <?php endif; // end if value?>
                            <!-- tube -->
                            <div style="padding-top:15px;">
                                <?php if (get_field('nearest_tube')) { ?>
                                <p class="nearest-tube"><?php the_field('nearest_tube'); ?></p>
                                <?php } // end if value?>
                            </div>
                            <span class="tellmore">Tell me more</span>
                        </div>

                    </a>
                </div>
                <?php endwhile; ?>
                <!-- end of the loop -->
                <?php wp_reset_postdata(); ?>
            </div>
            <?php else : ?>
            <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
        </div>

    </div>
    </div>
</section>

<div id="becomemember" style="height:auto;">
    <div class="container">
        <div class="row">
            <div class="col-lg">
                <h1>Become a Member</h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p>Get full access to the City A.M. Club membership for<br> £240/year or £20/month (incl VAT)<br></p>
                <div class="row">
                    <button type="button" id="viewpartners" class="btn btn-primary btn-lg"><a
                            href="<?php echo esc_url( home_url() ); ?>/register/">Join today</a></button>
                </div>
                <br>
                <div class="row">
                    <p><a href="mailto:events@cityam.com?Subject=Membership%20enquiry" class="membership-email"
                            target="_top">Enquire about corporate membership</a><a href=""></a></p>
                </div>
            </div>
        </div>

    </div>
    <h3>Why Join ?</h3>
</div>
<div class="row" style="margin-bottom:80px">

    <div class="toggle col-lg-8">

        <?php if( have_rows('why_join') ): ?>
        <?php while( have_rows('why_join') ): the_row(); 
                                                        $toggle_id = get_sub_field('toggle_id');
                                                        $toggle_title = get_sub_field('toggle_title');
                                                        $toggle_text = get_sub_field('toggle_text');

                    								?>


        <div class="col-sm-6 togglebox">
            <p>
                <a class="togglebutton collapsed" data-toggle="collapse" href="#multiCollapse<?php echo $toggle_id;?>"
                    role="button" aria-expanded="false" aria-controls="multiCollapse1">
                    <?php echo $toggle_title;?></a>
            </p>
            <div class="multi-collapse collapse" id="multiCollapse<?php echo $toggle_id;?>" style="">
                <div class="card card-body">
                    <?php echo $toggle_text;?>
                </div>
            </div>
        </div>

        <?php endwhile; ?>
        <?php endif; ?>

    </div>
</div>