<?php
/**
 * Template part for displaying page content in user page feed
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

$user_id = get_current_user_id();
$interests = get_field('interest','user_'.$user_id);
$event_interests = get_field('event_interest','user_'.$user_id);
$args = array(
	'posts_per_page' => 6,
	'eventDisplay' => 'mixed' // only upcoming
);
if( ( !empty($interests) && count($interests) > 0 ) || ( !empty($event_interests) && count($event_interests) > 0 ) ){
	$args['tax_query']['relation'] = "OR";
}
if( ( !empty($interests) && count($interests) > 0 ) ){
	foreach ($interests as $key => $interest) {
		$args['tax_query'][] = array(
			'taxonomy' => $interest->taxonomy,
			'field'    => 'term_id',
			'terms'    => array($interest->term_id),
		);
		
	}
}
if( ( !empty($event_interests) && count($event_interests) > 0 ) ){
	foreach ($event_interests as $key => $event_interest) {
		$args['tax_query'][] = array(
			'taxonomy' => $event_interest->taxonomy,
			'field'    => 'term_id',
			'terms'    => array($event_interest->term_id),
		);
		
	}
}
$proj_events = tribe_get_events( $args, true ); ?>

<section id="personalfeed" class="userfeed">
	<div class="container">
		<div class="row">
			<h2 style="margin:auto; color: #2D4560;">Your Personalised Events</h2>
			<?php if( $proj_events->have_posts() ) : ?>
				<div class="card-columns">
					<?php while( $proj_events->have_posts() ) : $proj_events->the_post(); ?>
						<div class="col card">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><h3>	
							<?php echo tribe_events_event_schedule_details(); ?>
							<div class="feed">
								<?php echo tribe_event_featured_image( null, 'medium' ); ?>
							</div>
							<?php echo tribe_events_get_the_excerpt(); ?>
						</div>
						<a href="<?php echo esc_url( tribe_get_event_link() ); ?>"></a>
					<?php endwhile; ?>
				</div><!-- card-columns -->
			<?php else: ?>
				<p>There are currently no upcoming events for <?php the_title(); ?>.</p>
			<?php endif; ?> 
			<?php wp_reset_postdata(); ?>
		</div><!-- row -->
 	</div><!-- container -->
</section>
 

