<?php
/**
 * Template part for displaying page content in user page feed
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>
<section id="personalfeed">
<div class="container">
<div class="row">

<div class="card-columns">
			<?php 
			$args = array(
				'post_type' => array('partner','event'),
				'orderby'   => 'rand',
				'posts_per_page' => 6, 
			);
			$query = new WP_Query( $args );
			?>
		<?php // the query 
		$the_query = new WP_Query( $args ); ?>

	<?php if ( $the_query->have_posts() ) : ?>
	<!-- the loop -->
    	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    		<div class="col card">
				<a href="<?php the_permalink(); ?>">
				
		<div class="feed">
	
			<div class="overlay">
				<?php the_post_thumbnail(); ?>
			</div>
       		<img src="<?php the_field('event_image'); ?>"/><img src="<?php the_field('partner_bio_image'); ?>"/> 
 		<!-- start date/time -->	<h2><?php the_field('name'); ?></p></h2>
		<div>
				<?php 
				if (get_field('start_date')) {
					?>
						<p class="date"><?php the_field('start_date'); ?></p>
						<?php 
				} // end if value
				?>
		</div>
		<!-- tube -->
		<div style="padding-top:15px;">
				<?php if (get_field('nearest_tube')) { ?>
				<p class="nearest-tube"><?php the_field('nearest_tube'); ?></p>
				<?php } // end if value?>
		</div>
		</a>
		<span class="tellmore"><a href="<?php the_permalink(); ?>">Tell me more</a></span>
			</div>
		</div>
	<?php endwhile; ?>
	<!-- end of the loop -->
	<?php wp_reset_postdata(); ?>
        </div>
<?php else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
	</div>
 
</div> 
	</div>
</section>
 

