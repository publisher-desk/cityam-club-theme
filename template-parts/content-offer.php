<?php
/**
 * Template part for displaying partner offers in home page feed
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */
$tax = array('relation' => 'OR');
$args = array('post_type' => 'partner', 'posts_per_page' => 10, 'orderby' => 'rand',);
if(is_user_logged_in()){
	$user_id = get_current_user_id();
	$interests = get_field('interest','user_'.$user_id);

	foreach ($interests as $key => $interest) {
		if(!is_object($interest)){
			$interest = get_term_by( 'term_taxonomy_id', $interest );
		}
		$tax[] = array(
			'taxonomy' => $interest->taxonomy,
			'field'    => 'term_id',
			'terms'    => array($interest->term_id),
			'orderby'   => 'rand',
		);
		
	}
	$args['tax_query'] = $tax;
}
?>

<?php $parenters = new WP_Query($args);?>
<?php if($parenters->have_posts()) :?>

<div>
	
	<div class="row">
		<div class="col">
			<h1 class="happening-now-title">Happening now!</h1>
			<p class="happening-now-description">Every week we gather together all the greatest events and offers from the City AM Club – see below why Its Better on the Inside</p>
		</div>
	</div><!-- row -->

	<div class="row">
		<div class="col">

			<div class="happening-now-carousel marquee">
				<?php while($parenters->have_posts()): ?>

					<?php $parenters->the_post(); ?>
					<div class="happening-now-slide-item">
						<a href="<?php echo get_the_permalink(); ?>">
							<?php

							if ( has_blocks( get_the_content() ) ) {
								$blocks = parse_blocks( get_the_content() );
								for ($i=0; $i < count($blocks); $i++) { 
									if ( $blocks[$i]['blockName'] === 'tpd/block-tpd-container-carousel' ) {
										// error_log( print_r( $blocks[$i], true ) );
										$html = $blocks[$i]['innerBlocks'][0]['innerHTML'];
										preg_match_all('/<img[^>]+>/i', $html, $img);
										preg_match_all('/(?<=<h2\sclass="offer_title">)(.*)(?=<\/h2)+/i', $html, $title);
										preg_match_all('/(?<=<span\sclass="offer_copy"><p>)(.*)+/i', $html, $desc); //needs work
										// error_log( print_r( $desc, true ) );
										echo $img[0][0];
										$title = ( isset($title[0][0]) ) ? $title[0][0] : '' ;
										echo "<h3 class='offer-title'>".$title."</h3>";
										$desc = ( isset($desc[0][0]) ) ? $desc[0][0] : '' ;
										echo "<p class='offer-details'>".$desc."</p>";
									}
								}
							}
							/*
							<h3 class="offer-title"><?php the_title(); ?></h3>
							<p class="offer-details">offer description here</p>
							*/
							?>
						</a>
					</div>

				<?php endwhile; ?>
				<?php wp_reset_postdata();?>
			</div><!-- happening-now-carousel -->

		</div>
	</div>

</div>

<?php endif; ?>