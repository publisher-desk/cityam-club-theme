<div class="container events-header">
    <div class="row">
        <div class="col">
            <div class="tribe-events-intro-container ">
                <h1 class="tribe-events-title text-left"><?php echo tribe_get_events_title() ?></h1>
                <p class="tribe-events-description">The City A.M Club is your passport to the City, offering you exclusive access to London’s most enticing and exciting experiences.</p>
				<?php //tribe_get_template_part( 'modules/bar' ); ?>
			</div>
        </div>
    </div>
</div>