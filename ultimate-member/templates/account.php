<div class="um <?php echo $this->get_class( $mode ); ?> um-<?php echo esc_attr( $form_id ); ?>">

	<div class="um-form">
	
		<form method="post" action="">
			
			<?php
			/**
			 * UM hook
			 *
			 * @type action
			 * @title um_account_page_hidden_fields
			 * @description Show hidden fields on account form
			 * @input_vars
			 * [{"var":"$args","type":"array","desc":"Account shortcode arguments"}]
			 * @change_log
			 * ["Since: 2.0"]
			 * @usage add_action( 'um_account_page_hidden_fields', 'function_name', 10, 1 );
			 * @example
			 * <?php
			 * add_action( 'um_account_page_hidden_fields', 'my_account_page_hidden_fields', 10, 1 );
			 * function my_account_page_hidden_fields( $args ) {
			 *     // your code here
			 * }
			 * ?>
			 */
			do_action( 'um_account_page_hidden_fields', $args ); ?>

			<div class="um-account-meta radius-<?php echo UM()->options()->get( 'profile_photocorner' ); ?> uimob340-show uimob500-show">

				<div class="um-account-meta-img">
					<a href="<?php echo um_user_profile_url(); ?>"><?php echo get_avatar( um_user('ID'), 120 ); ?></a>
				</div>

				<div class="um-account-name">
					<?php /* <a href="<?php echo um_user_profile_url(); ?>"><?php echo um_user( 'display_name' ); ?></a> */ ?>
					<div style="font-size:2em;"><?php echo um_user( 'display_name' ); ?></div>
					<div class="um-account-profile-link">
						<?php /* <a href="<?php echo um_user_profile_url(); ?>" class="um-link"><?php _e( 'View profile', 'ultimate-member' ); ?></a> */ ?>
					</div>
				</div>

			</div>
			
			<div class="um-account-side uimob340-hide uimob500-hide">

				<div class="um-account-meta radius-<?php echo UM()->options()->get('profile_photocorner'); ?>">

					<div class="um-account-meta-img uimob800-hide"><a href="<?php echo um_user_profile_url(); ?>"><?php echo get_avatar( um_user('ID'), 120 ); ?></a></div>

					<?php if ( UM()->mobile()->isMobile() ) { ?>

						<div class="um-account-meta-img-b uimob800-show" title="<?php echo um_user( 'display_name' ); ?>"><a href="<?php echo um_user_profile_url(); ?>"><?php echo get_avatar( um_user('ID'), 120 ); ?></a></div>

					<?php } else { ?>

						<div class="um-account-meta-img-b uimob800-show um-tip-w" title="<?php echo um_user( 'display_name' ); ?>"><a href="<?php echo um_user_profile_url(); ?>"><?php echo get_avatar( um_user('ID'), 120 ); ?></a></div>

					<?php } ?>

					<div class="um-account-name uimob800-hide">
						<?php /* <a href="<?php echo um_user_profile_url(); ?>"><?php echo um_user( 'display_name', 'html' ); ?></a> */ ?>
						<div style="font-size:2em;"><?php echo um_user( 'display_name', 'html' ); ?></div>
						<?php /* 
						<div class="um-account-profile-link"><a href="<?php echo um_user_profile_url(); ?>" class="um-link"><?php _e( 'View profile', 'ultimate-member' ); ?></a></div>
						*/ ?>
					</div>

				</div>

				<ul>
					<?php foreach ( UM()->account()->tabs as $id => $info ) {
						if ( isset( $info['custom'] ) || UM()->options()->get( "account_tab_{$id}" ) == 1 || $id == 'general' ) { ?>

							<li>
								<a data-tab="<?php echo $id ?>" href="<?php echo UM()->account()->tab_link( $id ); ?>" class="um-account-link <?php if ( $id == UM()->account()->current_tab ) echo 'current'; ?>">
									<?php if ( UM()->mobile()->isMobile() ) { ?>
										<span class="um-account-icontip uimob800-show" title="<?php echo $info['title']; ?>"><i class="<?php echo $info['icon']; ?>"></i></span>
									<?php } else { ?>
										<span class="um-account-icontip uimob800-show um-tip-w" title="<?php echo $info['title']; ?>"><i class="<?php echo $info['icon']; ?>"></i></span>
									<?php } ?>

									<span class="um-account-icon uimob800-hide"><i class="<?php echo $info['icon']; ?>"></i></span>
									<span class="um-account-title uimob800-hide"><?php echo $info['title']; ?></span>
									<span class="um-account-arrow uimob800-hide">
										<i class="<?php if ( is_rtl() ) { ?>um-faicon-angle-left<?php } else { ?>um-faicon-angle-right<?php } ?>"></i>
									</span>
								</a>
							</li>	

						<?php }
					} ?>
					
					<?php if ( wcs_user_has_subscription( get_current_user_id(), '', 'cancelled' ) || wcs_user_has_subscription( get_current_user_id(), '', 'active' ) ): ?>
					<li>
						<a data-tab="subscription" href="http://localhost/club-cityam/account/subscription/" class="um-account-link ">
							<span class="um-account-icontip uimob800-show um-tip-w" original-title="Privacy"><i class="um-faicon-tag"></i></span>
							<span class="um-account-icon uimob800-hide"><i class="um-faicon-tag"></i></span>
							<span class="um-account-title uimob800-hide">Subscription</span>
							<span class="um-account-arrow uimob800-hide">
								<i class="um-faicon-angle-right"></i>
							</span>
						</a>
					</li>
					<?php endif; ?>

		
				</ul>

				<?php $user = wp_get_current_user(); ?>
				<?php if ( !wcs_user_has_subscription( get_current_user_id(), '', 'active' ) && !wcs_user_has_subscription( get_current_user_id(), '', 'cancelled' ) ): ?>
					<!-- hide and show membership link -->
					<p id="memberpay" class="button">
						<a href="<?php echo esc_url( home_url() ); ?>/product/membership" class="btn btn-primary" data-small-btn="true">Get full membership</a>
					</p>
				<?php endif; ?>

			</div>
		
			<div class="um-account-main" data-current_tab="<?php echo UM()->account()->current_tab; ?>">
			
				<?php
				/**
				 * UM hook
				 *
				 * @type action
				 * @title um_before_form
				 * @description Show some content before account form
				 * @input_vars
				 * [{"var":"$args","type":"array","desc":"Account shortcode arguments"}]
				 * @change_log
				 * ["Since: 2.0"]
				 * @usage add_action( 'um_before_form', 'function_name', 10, 1 );
				 * @example
				 * <?php
				 * add_action( 'um_before_form', 'my_before_form', 10, 1 );
				 * function my_before_form( $args ) {
				 *     // your code here
				 * }
				 * ?>
				 */
				do_action( 'um_before_form', $args );
				
				foreach ( UM()->account()->tabs as $id => $info ) {

					$current_tab = UM()->account()->current_tab;

					if ( isset( $info['custom'] ) || UM()->options()->get( 'account_tab_' . $id ) == 1 || $id == 'general' ) { ?>

						<div class="um-account-nav uimob340-show uimob500-show">
							<a href="#" data-tab="<?php echo $id; ?>" class="<?php if ( $id == $current_tab ) echo 'current'; ?>">
								<?php echo $info['title']; ?>
								<span class="ico"><i class="<?php echo $info['icon']; ?>"></i></span>
								<span class="arr"><i class="um-faicon-angle-down"></i></span>
							</a>
						</div>

						<div class="um-account-tab um-account-tab-<?php echo $id ?>" data-tab="<?php echo $id ?>">
							<?php $info['with_header'] = true;
							UM()->account()->render_account_tab( $id, $info, $args ); ?>
						</div>

					<?php }
				} ?>

				<?php // custom code for subscriptions ?>				
				<div class="um-account-nav uimob340-show uimob500-show">
					<a href="#" data-tab="subscription" class="">
						Subscription<span class="ico"><i class="um-faicon-tag"></i></span><span class="arr"><i class="um-faicon-tag"></i></span>
					</a>
				</div>

				<?php if ( wcs_user_has_subscription( get_current_user_id(), '', 'cancelled' ) || wcs_user_has_subscription( get_current_user_id(), '', 'active' ) ): ?>
				<div class="um-account-tab um-account-tab-subscription" data-tab="subscription" style="display: none;">							
					<div class="um-account-heading uimob340-hide uimob500-hide"><i class="um-faicon-tag"></i>Subscription</div>
					<?php 
						$subscription = wcs_get_users_subscriptions(get_current_user_id());
						$subscription = reset($subscription);
						do_action( 'woocommerce_subscription_details_table', $subscription );
						?>
					<div class="um-clear"></div>
				</div>
				<?php endif; ?>
				<?php /* ************************** */ ?>				




			</div>
			<div class="um-clear"></div>
		</form>
		
		<?php
		/**
		 * UM hook
		 *
		 * @type action
		 * @title um_after_account_page_load
		 * @description After account form
		 * @change_log
		 * ["Since: 2.0"]
		 * @usage add_action( 'um_after_account_page_load', 'function_name', 10 );
		 * @example
		 * <?php
		 * add_action( 'um_after_account_page_load', 'my_after_account_page_load', 10 );
		 * function my_after_account_page_load() {
		 *     // your code here
		 * }
		 * ?>
		 */
		do_action( 'um_after_account_page_load' ); ?>
	
	</div>

</div>