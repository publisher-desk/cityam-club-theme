<div style="max-width: 560px;padding: 20px;background: #2d4560;border-radius: 5px;margin:40px auto;font-family: Open Sans,Helvetica,Arial;font-size: 15px;color: #f4e3db;">

	<div style="color: #444444;font-weight: normal;">
		<div style="text-align: center;font-weight:600;font-size:26px;padding: 10px 0;border-bottom: solid 1px #eeeeee;"><img style="width:100%;max-width:100px" class="custom-logo" src="<?php echo get_template_directory_uri(); ?>/assets/svg/full_logo-alt.svg" alt="" srcset=""></div>
		
		<div style="clear:both"></div>
	</div>
	
	<div style="padding: 0 30px 30px 30px;border-bottom: 3px solid #eeeeee;">

		<div style="padding: 30px 0;font-size: 24px;text-align: center;line-height: 40px;">You recently changed the password associated with your account.</div>

		<div style="padding: 10px 0 50px 0;text-align: center;"><a href="{user_account_link}" style="background: #ecb69b;color: #2d4560;padding: 12px 30px;text-decoration: none;border-radius: 0;letter-spacing: 0.3px;text-transform:uppercase;font-weight:700;">Go to your Account</a></div>
		
		<div style="padding: 15px;border-radius: 3px;text-align: center;">If you did not make this change and believe your account has been compromised, please <a href="mailto:{admin_email}" style="color: #ecb69b;text-decoration: none;">contact  us</a> ASAP.</div>
		
	</div>
	
	<div style="color: #999;padding: 20px 30px">
		
		<div style="">Thank you!</div>
		<div style="">The <a href="{site_url}" style="color: #ecb69b;text-decoration: none;">{site_name}</a> Team</div>
		
	</div>

</div>