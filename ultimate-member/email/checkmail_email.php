<div style="max-width: 560px; padding: 20px; background: #2d4560; border-radius: 5px; margin: 40px auto; font-family: Open Sans,Helvetica,Arial; font-size: 15px; color: #f4e3db;">
<div style="color: #444444; font-weight: normal;">
<div style="text-align: center; font-weight: 600; font-size: 26px; padding: 10px 0; border-bottom: solid 3px #eeeeee;"><img class="custom-logo" style="width: 100%; max-width: 100px;" src="http://dev.clubcityam.com/wp-content/themes/cityam-club-theme/assets/svg/full_logo-alt.svg" srcset="" alt="" /></div>
<div style="clear: both;"> </div>
</div>
<div style="padding: 0 30px 30px 30px; border-bottom: 3px solid #eeeeee;">
<div style="padding: 30px 0; font-size: 24px; text-align: center; line-height: 40px;">Thank you for signing up!<span style="display: block;">Please click the following link to activate your account.</span></div>
<div style="padding: 10px 0 50px 0; text-align: center;"><a style="background: #ecb69b; color: #2d4560; padding: 12px 30px; text-decoration: none; border-radius: 0; letter-spacing: 0.3px; text-transform: uppercase; font-weight: bold;" href="{account_activation_link}">Activate your Account</a></div>
<div style="padding: 15px; border-radius: 3px; text-align: center;">Need help? <a style="color: #ecb69b; text-decoration: none;" href="mailto:{admin_email}">contact us</a> today.</div>
</div>
<div style="color: #999; padding: 20px 30px;">
<div>Thank you!</div>
<div>The <a style="color: #ecb69b; text-decoration: none;" href="{site_url}">{site_name}</a> Team</div>
</div>
</div>