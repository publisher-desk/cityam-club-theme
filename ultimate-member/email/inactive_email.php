<div style="max-width: 560px; padding: 20px; background: #2d4560; border-radius: 5px; margin: 40px auto; font-family: Open Sans,Helvetica,Arial; font-size: 15px; color: #f4e3db;">
<div style="color: #444444; font-weight: normal;">
<div style="text-align: center; font-weight: 600; font-size: 26px; padding: 10px 0; border-bottom: solid 1px #eeeeee;"><img class="custom-logo" style="width: 100%; max-width: 100px;" src="http://dev.clubcityam.com/wp-content/themes/cityam-club-theme/assets/svg/full_logo-alt.svg" srcset="" alt="" /></div>
<div style="clear: both;"> </div>
</div>
<div style="padding: 0 30px 30px 30px; border-bottom: 1px solid #eeeeee;">
<div style="padding: 30px 0; font-size: 24px; text-align: center; line-height: 40px;">Your account is now deactivated.</div>
<div style="padding: 15px; border-radius: 3px; text-align: center;">If you want your account to be re-activated, please <a style="color: #ecb69b; text-decoration: none;" href="mailto:{admin_email}">contact us</a>.</div>
</div>
<div style="color: #999; padding: 20px 30px;">
<div>Thank you!</div>
<div>The <a style="color: #ecb69b; text-decoration: none;" href="{site_url}">{site_name}</a> Team</div>
</div>
</div>