<?php
/**
 * Template Name: Lightbox
 */

get_header();
?>
<section id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <?php
             $content = get_the_content(); echo $content;

            ?>
        </main><!-- #main -->
	</section><!-- #primary -->
	
 

<?php
get_footer();
