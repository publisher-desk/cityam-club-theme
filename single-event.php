<?php

get_header(); ?>




<div class="container-fluid">



    <section data-scroll>
        <div class="row">
            <div id="bio" class="col-lg-6 pt-5">
                <div class="offset-md-2">
                    <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
                    <p>
						<svg class="views-Icons-__icon_date views-Event-__date_icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.88 19.13"><title>Date Icon</title><g><g id="Layer_1" data-name="Layer 1"><rect class="views-Icons-__icon_date_cls_1" x="2.83" y="2.24" width="15.83" height="15.89"></rect><polyline class="views-Icons-__icon_date_cls_1" points="0 7.61 21.88 7.61 2.32 7.61"></polyline><line class="views-Icons-__icon_date_cls_1" x1="7.68" x2="7.68" y2="4.44"></line><line class="views-Icons-__icon_date_cls_1" x1="13.88" y1="0.09" x2="13.88" y2="4.53"></line></g></g></svg>
						<?php the_field('start_date'); ?>
					</p>
                    <p><?php the_field('event_location'); ?></p>
                    <?php $content = get_the_content();
					echo $content; ?>
                    <p style="float:left;">Members fee: <?php the_field('members_price'); ?> </p>
                    <p>Guests fee: <?php the_field('guest_price'); ?></p>
                    <span style="color: #d31132; font-size:13px;"><?php echo get_field('offer_note'); ?></span>

                    <p class="button">
                        <a data-fancybox data-type="iframe" href="<?php echo esc_url( home_url() ); ?>/cart"
                            class="btn btn-primary" data-small-btn="true" data-iframe='{"preload":false}'>
                            Book tickets
                        </a>
                    </p>
                    <p> save for later </p>

                </div>
                <div id="offer" class="offset-md-2">
                    <p class="event-address"><?php the_field('event_address'); ?></p>
                    <p class="nearest-tube"><?php the_field('nearest_tube'); ?></p>
                    <button class="phonebutton"><a class="site hvr-float button"
                            href="tel:+<?php echo get_field('phone'); ?>"
                            target='_blank;'>+<?php echo get_field('phone'); ?></a>
                        <button class="emailbutton"><a class="site hvr-float button"
                                href="mailto:<?php echo get_field('email'); ?>"
                                target='_blank;'><?php echo get_field('email'); ?></a>
                </div>
                <div class="socials col-lg-5" style="display:inline-flex;"><span style="color:#028080;">social
                        media</span>
                    <div class="fb"></div>
                    <div class="twitter"></div>
                    <div class="linkedin"></div>
                </div>
                <div class="partnership offset-md-2">
                    <?php if (have_rows('partnership_image')) : ?>
                    <p>In Partnership with </p>
                    <div class="post-thumbnail">
                        <div class="overlay"><?php the_post_thumbnail(); ?></div>
                    </div>
                    <?php while (have_rows('partnership_image')) : the_row();
								$partnerimage = get_sub_field('partnership_image');
								?>
                    <img src="<?php echo $partnerimage['url']; ?>" alt="<?php echo $partnerimage['alt'] ?>" />
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div id="bioimage" class="col-lg-6">
                <img src="<?php the_field('event_image'); ?>" />
            </div>
        </div>
    </section>

    <section data-scroll>
        <div id="eventoffer" class="row">
            <div class="col-lg-7">
                <div class="socials col-lg-1 socials col-lg-1 offset-md-2">Map</div>

            </div>

        </div>
    </section>
    <?php
	get_footer();