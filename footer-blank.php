</div><!-- #page -->
		
<?php /* https://css-tricks.com/need-to-scroll-to-the-top-of-the-page/ */ ?>
<a href="#top" onclick="window.scroll({ top: 0, left: 0, behavior: 'smooth'});" style="display:none;z-index:10;">
	<img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/up_arrow.png" alt="back to top">
</a>

<?php wp_footer(); ?>
</body>
</html>
