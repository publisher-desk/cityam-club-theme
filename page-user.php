<?php
wp_redirect(get_option('home'), 301); 
/**
 * Template Name: User
 */

get_header();
?>
	<div id="content-fluid" class="container-fluid site-content">
		<div class="container-fluid">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-bootstrap-starter' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->

         </div>
    </div>
 
    <div class="mainbody container-fluid" style="background-color:#F4E2DB;">
                  <?php
                while ( have_posts() ) : the_post();
                    get_template_part( 'template-parts/content-userfeed', 'notitle' );
                endwhile; // End of the loop.
                ?>
            </div>

<?php
get_footer();
